package com.sugardaddy.citascasualesconocergentenuevacercadeti.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.sugardaddy.citascasualesconocergentenuevacercadeti.R;

public class ClassifiedActivity extends AppCompatActivity {
    String ButtonID;
    Button btn_register;
    Button btn_login;
    Button btn_ads;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classified);
        ButtonID = getIntent().getStringExtra("ButtonId");
        btn_login = findViewById(R.id.btn_login_ads);
        btn_ads = findViewById(R.id.btn_ads);
        btn_ads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ClassifiedActivity.this, ClassifiedMenu.class);
                startActivity(intent);
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ClassifiedActivity.this, ClassifiedMainActivity.class);
                intent.putExtra("ButtonID", "Login");
                startActivity(intent);

            }
        });
        btn_register = findViewById(R.id.btn_register_ads);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ClassifiedActivity.this, ClassifiedMainActivity.class);
                intent.putExtra("ButtonID", "Register");
                startActivity(intent);

            }
        });
    }
}