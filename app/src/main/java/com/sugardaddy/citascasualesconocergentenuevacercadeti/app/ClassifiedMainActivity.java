package com.sugardaddy.citascasualesconocergentenuevacercadeti.app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.sugardaddy.citascasualesconocergentenuevacercadeti.Fragments.ClassifiedLoginFragment;
import com.sugardaddy.citascasualesconocergentenuevacercadeti.Fragments.RegistrationFragment;
import com.sugardaddy.citascasualesconocergentenuevacercadeti.R;

public class ClassifiedMainActivity extends AppCompatActivity {
String ButtonID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classified_main);
        ButtonID = getIntent().getStringExtra("ButtonID");

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (this.ButtonID.equals("Register")) {
            fragmentTransaction.add((int) R.id.Container, (Fragment) new RegistrationFragment());
            fragmentTransaction.commit();
        }
        else if (ButtonID.equals("Login"))
        {
            fragmentTransaction.replace((int) R.id.Container, (Fragment) new ClassifiedLoginFragment());
            fragmentTransaction.commit();
        }
    }
}