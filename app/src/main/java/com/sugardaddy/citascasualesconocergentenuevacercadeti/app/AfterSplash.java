package com.sugardaddy.citascasualesconocergentenuevacercadeti.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.sugardaddy.citascasualesconocergentenuevacercadeti.R;


public class AfterSplash extends AppCompatActivity {
Button btn_dateZone;
Button btn_classified_ads;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_splash);
        btn_dateZone=findViewById(R.id.btn_date_zone);
         btn_classified_ads=findViewById(R.id.btn_classified_ads);
        btn_dateZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AfterSplash.this, DateZoneWelcome.class);
                startActivity(intent);
            }
        });
        btn_classified_ads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(AfterSplash.this,ClassifiedActivity.class);
                intent.putExtra("ButtonId", "Register");
                intent.putExtra("ButtonId", "Login");
                intent.putExtra("ButtonId", "Ads");


                startActivity(intent);
            }
        });
    }
}