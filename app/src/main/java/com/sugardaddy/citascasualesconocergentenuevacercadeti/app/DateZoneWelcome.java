package com.sugardaddy.citascasualesconocergentenuevacercadeti.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.sugardaddy.citascasualesconocergentenuevacercadeti.R;
import com.sugardaddy.citascasualesconocergentenuevacercadeti.auth.WelcomeActivity;

public class DateZoneWelcome extends AppCompatActivity {
Button btn_dating_service;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_zone_welcome);
        btn_dating_service=findViewById(R.id.btn_dating_service);
        btn_dating_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(
                        DateZoneWelcome.this,
                        WelcomeActivity.class
                ));
            }
        });
    }
}